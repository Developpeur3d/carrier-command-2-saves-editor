# CARRIER COMMAND 2 saves editor #

This application is made for the players of [Carrier Command 2 game (available on Steam)](https://store.steampowered.com/app/1489630/Carrier_Command_2/).
It allow lazy players to ease their life by editing/tweaking saved games.
- SOLO ONLY -

### Features ###

* Change all inventory stock in the carrier (ammo, turrets, large munitions, chassis...)
* Change loaded weapon's ammo (160mm canon/ cruise missiles)
* Unlock all blueprints
* Destroy (or resurrect) ACC Omega
* Change islands allegiance
* Move islands on the map
* Customise carriers and vehicles colors
* Create backup of original save file

### Project ###

This application was made with [Unity 3d (C#)](https://unity.com/). It was born from a big frustration from the game which is too much difficult for me.
I played the original Carrier Command game on Atari ST at the time (more than 30 years ago). It is still today, one of my favorite games.
So naturally, I decided to go on Carrier Command 2. The game is quite good, the mood reminds me very much about the first opus. But, since I don't have much time today to spend on a game, I decided to tweak it a little.

The application allows you to edit saves slots from the game. You can modify the carrier stock in order to have unlimited resources.
I've not tested on multiplayer, only solo. So I can't garanty it works in every cases.
I made it to have fun in solo, nothing else.

Hope this will bring you much fun !

### Screenshots ###

![Ammo](https://www.developpeur3d.com/downloads/cc2saveseditor/screen1.jpg)
	
![Air chassis](https://www.developpeur3d.com/downloads/cc2saveseditor/screen2.jpg)
	
![Credits](https://www.developpeur3d.com/downloads/cc2saveseditor/screen3.jpg)
	
![Map](https://www.developpeur3d.com/downloads/cc2saveseditor/screen5.jpg)

### DOWNLOAD ###

You can download the application **[here (Windows x86/x64)](https://www.developpeur3d.com/downloads/cc2saveseditor/cc2saveseditor.zip)**
	
### How to use it ? ###

Download the zip file above. Unzip it.
Of course, you must have Carrier Command 2 installed on your computer in order for the app to work (unless you will have an error).
The saved slots are automatically read. You can select it in the drop down list (the first one is selected).
You can then modify whatever you want in the inventory.
The blueprints can be all unlocked, but it can be done once. If you want to roll back. Recover the backup. I will probably add a button to do it easily from the application. Tell me if you really need it.
The map can be manipulated with the scrollwheel (zoom) and drag'n drop. Click on an island to assign it to you (ACC Epsilon in blue), you ennemy (ACC Omega in yellow), or neutral (in red). You can drag'n drop island to move it on the map. Simple as that.

Don't forget to save it...
Reload the save from the game... and blast them all !

### Source code ###

The old crt effect is done by a paid asset which I cannot embed in the source code. Whereas it is of course available on the compiled binary, it is not in the source code.
But if you would like to embed it yourself feel free to catch ["Ultimate CRT" on Unity asset store](https://assetstore.unity.com/packages/vfx/shaders/fullscreen-camera-effects/ultimate-crt-80048)
	
### How it works ###

The Carrier Command 2 saved games are stored in xml files (C/Users/"your comp name"/AppData/Roaming/Carrier Command 2/saved_games/"your game slot").
The application change them to tweak carrier storage.

A backup of the original xml file is made at the first save... we never know.
Microprose can patch the game at any time which would prevent the application to work correctly. So I cannot be held reponsible for the damage the application could do to your files. Use it at your own risk.
If you see any changes or glitches feel free to contact me (below).

### Troubleshooting ###

If the value is not updated, it might be because you are using an old save xml. Please, save your game just before trying to modify it.

### Contact ###

I don't have much time to spend on this application. But if you notice a bug. Do not hesitate to [contact me](https://www.developpeur3d.com/contact.php?lg=en)

### Version history ###

**v0.20**

* Updated project to Unity 6
* Geometa/Microprose changed carrier colors : updated

**v0.19**

* fixed error when saving

**v0.18**

* Added 30mm gimbal ((in "Turrets" menu
* Added total weight
* Update to Unity 2022.2.15f1

**v0.17**

* Added "deployable drone" and "Mule chassis" since major update 4 (september 2022)
* Update to Unity 2021.3.11f1

**v0.16**

* Added color customisation (thanks to **[Zfall99](https://steamcommunity.com/sharedfiles/filedetails/?id=2573613402)** and kykeluna)

**v0.15**

* Added loaded state of carrier's canon (160mm ammo) and cruise missiles (see screenshot below for explainations)
![Loaded weapons](https://www.developpeur3d.com/downloads/cc2saveseditor/screen6.jpg)

* fixed a bug in inputfields



**v0.14**

* Application is now resizable. So, the UI is fetching itself.
* The app is now checking regularly if it is up to date. Not only when clicking on the "?".
* some minor bug fixes

**v0.13**

* Minor bug fixes

**v0.12**

* new feature : move island on map (experimental feature)
* Fix decreased Crt effect which was a bit too strong
* Fix better management of error at load time of defective/incompatible slots.

**v0.11**

* fix some issues when quiting the application

**v0.1**

* map editor
* inventory editor
* fuel
* currency...
* unlocking blueprint
* killing/resurecting ACC Omega
* backup system of original xml file

