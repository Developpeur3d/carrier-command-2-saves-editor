﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/**
 *      Panneau de notification pour les message visant à remplacer le panel de notification natif (Native toolkit).
 */
public class Notification : MonoBehaviour {

    // Le composant de texte permettant d'afficher le titre
    public Text titreComponent;
    // Le composant de texte permettant d'afficher les données.
    public Text textComponent;
    // Le composant de texte du bouton OK
    public Text textBoutonOKComponent;
    // Le composant de texte du bouton d'annulation
    public Text textBoutonCANCELComponent;
    // Le composant de texte du bouton de confirmation
    public Text textBoutonCONFIRMComponent;
    // Le fond (sur l'ensemble de l'écran et qui empèche de cliquer ailleurs)
    public GameObject backgroundGameObject;

    /**
     *  Le descriptif
     */
    public string texte {
        get {
            return textComponent.text;
        }
        set {
            textComponent.text = value;
        }
    }

    /**
     *  Le titre
     */
    public string titre {
        get {
            return titreComponent.text;
        }
        set {
            titreComponent.text = value;
            // S'il n'y a pas de titre, on le supprime.
            if (value=="") {
                titreComponent.gameObject.SetActive(false);
            } else if(!titreComponent.gameObject.activeSelf) {
                titreComponent.gameObject.SetActive(true);
            }
        }
    }

    /**
     *  Le fond qui s'affiche sur l'ensemble de l'écran.
     */
    public bool background {
        get {
            return backgroundGameObject.activeSelf;
        }
        set {
            backgroundGameObject.SetActive(value);
        }
    }

    /**
     *  L'intitulé du bouton OK
     */
    public string intitule_boutonOK {
        get {
            return textBoutonOKComponent.text;
        }
        set {
            textBoutonOKComponent.text = value;
        }
    }

    /**
     *  L'intitulé du bouton CONFIRM
     */
    public string intitule_boutonCONFIRM {
        get {
            return textBoutonCONFIRMComponent.text;
        }
        set {
            textBoutonCONFIRMComponent.text = value;
        }
    }

    /**
     *  L'intitulé du bouton ANNULATION
     */
    public string intitule_boutonANNULATION {
        get {
            return textBoutonCANCELComponent.text;
        }
        set {
            textBoutonCANCELComponent.text = value;
        }
    }

    public enum Types { ALERT, CONFIRM, SIMPLE};

    // Le type de notification
    public Types type {
        get {
            return _type;
        }
        set {
            _type = value;
            switch (value) {
                case Types.ALERT:
                default:
                    textBoutonCONFIRMComponent.transform.parent.gameObject.SetActive(false);
                    textBoutonCANCELComponent.transform.parent.gameObject.SetActive(false);
                    textBoutonOKComponent.transform.parent.gameObject.SetActive(true);
                    break;
                case Types.CONFIRM:
                    textBoutonOKComponent.transform.parent.gameObject.SetActive(false);
                    textBoutonCONFIRMComponent.transform.parent.gameObject.SetActive(true);
                    textBoutonCANCELComponent.transform.parent.gameObject.SetActive(true);
                    break;
                case Types.SIMPLE:
                    textBoutonOKComponent.transform.parent.gameObject.SetActive(false);
                    textBoutonCONFIRMComponent.transform.parent.gameObject.SetActive(false);
                    textBoutonCANCELComponent.transform.parent.gameObject.SetActive(false);
                    titreComponent.alignment = TextAnchor.MiddleCenter;
                    textComponent.alignment = TextAnchor.MiddleCenter;
                    break;
            }
        }
    }
    private Types _type;

    // Le callback qui doit être lancé à la fermeture de la fenêtre.
    public UnityAction callbackClose;
    // Le callback lancé lors de la confirmation.
    public UnityAction<bool> callbackConfirm;

    IEnumerator Start() {
        yield return new WaitForEndOfFrame();
        TweenCGAlpha.Tween(this.gameObject, 0.5f, 0f, 1f, () => {
            switch (type) {
                case Types.ALERT:
                case Types.CONFIRM:
                    Time.timeScale = 0f;
                    break;
                default:
                    Time.timeScale = 1f;
                    break;
            }
        });
    }

    #region METHODES PUBLIQUES
    /**
     *  Instantiation du message d'alerte
     */
    public static void Alert(string _titre, string _message, UnityAction _callback = null, string _intitule_bouton = "OK") {
        GameObject instance = Instantiate<GameObject>(Resources.Load<GameObject>("Notification"));
        Notification n = instance.GetComponent<Notification>();
        n.GetComponent<Canvas>().worldCamera = Camera.main;
        n.type = Types.ALERT;
        n.callbackClose = _callback;
        n.texte = _message;
        n.titre = _titre;
        n.StartCoroutine(n.CheckLongueurTexte());
        n.intitule_boutonOK = _intitule_bouton;
    }

    /**
     *  Instantiation d'une boite de confirmation.
     */
    public static void Confirm(string _titre, string _message, UnityAction<bool> _callback = null, string _intitule_boutonConfirm = "CONFIRMER", string _intitule_boutonAnnulation = "ANNULER") {
        GameObject instance = Instantiate<GameObject>(Resources.Load<GameObject>("Notification"));
        Notification n = instance.GetComponent<Notification>();
        n.type = Types.CONFIRM;
        n.GetComponent<Canvas>().worldCamera = Camera.main;
        n.callbackConfirm = _callback;
        n.texte = _message;
        n.titre = _titre;
        n.StartCoroutine(n.CheckLongueurTexte());
        n.intitule_boutonCONFIRM = _intitule_boutonConfirm;
        n.intitule_boutonANNULATION = _intitule_boutonAnnulation;
    }

    /**
     * Message affiché durant un délai de quelques secondes.
     */
    public static void Simple(string _titre, string _message, float _delay=3f, bool _background=false, UnityAction _callback=null) {
        GameObject instance = Instantiate<GameObject>(Resources.Load<GameObject>("Notification"));
        Notification n = instance.GetComponent<Notification>();
        n.type = Types.SIMPLE;
        n.texte = _message;
        n.GetComponent<Canvas>().worldCamera = Camera.main;
        n.titre = _titre;
        n.background = _background;
        n.callbackClose = _callback;
        n.StartCoroutine(n.CheckLongueurTexte());
        // On lance le timer avant de fermer la fenêtre.
        n.StartCoroutine(n.IE_Close(_delay));
       // n.Invoke("Close", _delay);
    }

    public IEnumerator IE_Close(float _delay) {
        yield return new WaitForSeconds(_delay);
        Close();
    }

    /**
     *      Fermeture de la notification.
     *      _noCallBack permet de ne pas lancer de callBack lorsqu'on force la fermeture de toutes les fenêtres de notification.
     */
    public void Close(bool _noCallBack=false) {
        //Debug.Log("Closing...");
        Time.timeScale = 1f;
        TweenCGAlpha.Tween(this.gameObject, 0.5f, 0f, delegate {
            if (callbackClose != null)
                callbackClose.Invoke();
            Destroy(this.gameObject);
        });
    }
    public void CloseConfirm() {
        if (callbackConfirm != null)
            callbackConfirm.Invoke(false);
        Close();
    }

    /// <summary>
    /// Ferme toutes les notifications ouvertes.
    /// </summary>
    public static void CloseAll() {
        FindObjectsOfType<Notification>().ToList<Notification>().ForEach(n => n.Close(true));
    }
    #endregion

    #region PRIVATE
    /**
     *      Validation d'une boite de confirmation.
     */
    public void ValidConfirm() {
        Time.timeScale = 1f;
        TweenCGAlpha.Tween(this.gameObject, 0.5f, 0f, delegate {
            if (callbackConfirm != null)
                callbackConfirm.Invoke(true);
            Destroy(this.gameObject);
        });
    }

    /**
     *      Vérifie la longueur du texte et ajuste la taille du panel si le texte est trop court.
     */
    public IEnumerator CheckLongueurTexte() {
        yield return new WaitForEndOfFrame();
        VerticalLayoutGroup content = textComponent.GetComponentInParent<VerticalLayoutGroup>();
        //Debug.Log(content.GetComponent<RectTransform>().rect.height +" | "+ content.transform.parent.GetComponent<RectTransform>().rect.height);
        if (content.GetComponent<RectTransform>().rect.height < content.transform.parent.GetComponent<RectTransform>().rect.height) {
            RectTransform panel = this.GetComponentsInChildren<Image>().Where<Image>(n => n.name == "Panel").FirstOrDefault().GetComponent<RectTransform>();
            float addendum = 50f;
            /*switch (type) {
                case Types.SIMPLE:
                    addendum = 50f;
                    break;
            }*/
            panel.sizeDelta = new Vector2(panel.sizeDelta.x, content.GetComponent<RectTransform>().sizeDelta.y + addendum);
            // On désactive du même coup le scrolling.
            GetComponentInChildren<ScrollRect>().vertical = false;
        }
    }

    #endregion

}
