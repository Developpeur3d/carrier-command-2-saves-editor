UGUITWeen
=========

All of the tween components can be found in UI/Tweening  of the components menu or can be accessed,
via the static method Tween on the TweenAlpha, TweenCGAlpha,
TweenPos, TweenRot, TweenScale classes.

Usage:
```
TweenPos.Tween(gameObject, 1f, vector3.zero);
```
