using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slowly rotate the target icon on the selected island.
/// </summary>

public class RotateTarget:MonoBehaviour {

    public float speed = 1f;

    void Update() {
        this.transform.Rotate(0f, 0f, -0.1f * speed);
    }
}
