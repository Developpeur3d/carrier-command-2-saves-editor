using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

/// <summary>
/// Intercept clicks on hyperlink in TextMeshPro components
/// credits : https://deltadreamgames.com/unity-tmp-hyperlinks/
/// </summary>

[RequireComponent(typeof(TextMeshProUGUI))]
public class OpenHyperlinks:MonoBehaviour, IPointerClickHandler {

    public void OnPointerClick(PointerEventData eventData) {
        TMP_Text pTextMeshPro = GetComponent<TMP_Text>();
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, Camera.main);
        if (linkIndex != -1) { // was a link clicked?
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];

            // open the link id as a url, which is the metadata we added in the text field
            Application.OpenURL(linkInfo.GetLinkID());
        }
    }
}