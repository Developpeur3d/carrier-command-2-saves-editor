using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

/// <summary>
/// Validate digit values from inputfields. It assures that the user cannot enter negatives values for example.
/// </summary>

[Serializable]
[CreateAssetMenu(fileName = "InputValidator - Digits.asset", menuName = "TextMeshPro/Input Validators/Digits", order = 100)]
public class Validator:TMP_InputValidator {
    public override char Validate(ref string text, ref int pos, char ch) {
        if(ch >= '0' && ch <= '9' && pos<9)
            {
            //text += ch;
            text = text.Substring(0, Mathf.Max(0,pos))+ch+ text.Substring(Math.Min(pos, text.Length));
            pos += 1;
            return ch;
        }
        return (char)0;
    }
}
