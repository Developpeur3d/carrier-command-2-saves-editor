using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Assertions;

/// <summary>
/// Positionne un �l�ment UI sur le pixelRect d'une cam�ra
/// � Cecil ISNARD - developpeur3d.com (2020)
/// </summary>

[ExecuteInEditMode()]
[RequireComponent(typeof(VisualElement))]
public class PixelRectToUI : MonoBehaviour {
    public Camera cam;
    public Vector2 offset = new Vector2(5f, -5f);
    private RectTransform rt;
    private Canvas canvas;

    void Start() {
        Assert.IsNotNull(cam);
        rt = this.transform as RectTransform;
        canvas = GetComponentInParent<Canvas>();
    }

    void Update() {
        if (cam == null) return;
        // La position
        rt.anchoredPosition = new Vector2((cam.pixelRect.x + offset.x) / canvas.scaleFactor, (cam.pixelRect.height - Screen.height + cam.pixelRect.y + offset.y) / canvas.scaleFactor);
        // la dimension
        //Debug.Log(cam.pixelRect.width + " / " + cam.pixelRect.height);
        rt.sizeDelta = new Vector2(cam.pixelRect.width/canvas.scaleFactor, cam.pixelRect.height/canvas.scaleFactor);
    }
}
