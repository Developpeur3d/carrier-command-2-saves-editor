using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;
using TMPro;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// Rollover on an island.
/// </summary>

public class IslandRollover:MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

    public GameObject prefab_rollover;
    private static GameObject roll_instance;
    /// <summary>
    /// The target to display when the island is selected
    /// </summary>
    public GameObject target;

    [Space(20f)]
    public string island_name;
    public int island_id;
    /// <summary>
    /// The team id of the island (to get whom the island owns : neutral, epsilon, omega).
    /// </summary>
    public int team_id = 0;
    /// <summary>
    /// The type of island
    /// </summary>
    public int island_type;

    // ============ EVENTS ================
    public delegate void AllegianceChanged(int _island_id, int _team_id);
    public static event AllegianceChanged EventAllegianceChanged;
    public static void OnAllegianceChanged(int _island_id, int _team_id) {
        if (EventAllegianceChanged != null)
            EventAllegianceChanged(_island_id, _team_id);
    }
    public delegate void Selected(int _island_id);
    public static event Selected EventSelected;
    public static void OnSelectIsland(int _island_id) {
        if (EventSelected != null)
            EventSelected(_island_id);
    }
    public delegate void UnSelected();
    public static event UnSelected EventUnSelected;
    public static void OnUnSelectIsland() {
        if (EventUnSelected != null)
            EventUnSelected();
    }
    // ====================================

    /// <summary>
    /// Indicates if we are dragging an island, and which one
    /// </summary>
    public static IslandRollover dragging;

    /// <summary>
    /// The current selected island id.
    /// (<0 if none)
    /// </summary>
    public static int current_island_id_selected = -1;


    /*private enum TYPE {
        WAREHOUSE=0,
        SMALL_MUNITIONS=1,
        LARGE_MUNITIONS=2,
        TURRET=3,
        UTILITY=4,
        SURFACE_CHASSIS=5,
        AIR_CHASSIS=6,
        FUEL=7,
        BARGE=8
    }*/

    /// <summary>
    /// The color of the neutral side.
    /// </summary>
    public static Color color_neutral = Color.red;
    /// <summary>
    /// The color of the ACC Epsilon side (player side)
    /// </summary>
    public static Color color_epsilon = new Color(22f/255f,238f/255f,248f/255f);
    /// <summary>
    /// The color of the ACC Omega side (ennemi side)
    /// </summary>
    public static Color color_omega = new Color(246f/255f, 214f/255f, 0f/255f);

    private TMP_Text component_island_name;

    void Start() {
#if UNITY_EDITOR
        Assert.IsNotNull(prefab_rollover);
        Assert.IsNotNull(target);
#endif
        component_island_name = GetComponentInChildren<TMP_Text>();
    }

    private void OnEnable() {
        EventAllegianceChanged += SetAllegiance;
        EventSelected += SelectMe;
        EventUnSelected += UnselectMe;
    }
    private void OnDisable() {
        EventAllegianceChanged -= SetAllegiance;
        EventSelected -= SelectMe;
        EventUnSelected -= UnselectMe;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        // Disabled rollover in v0.12
#if UNITY_EDITOR
        roll_instance = Instantiate<GameObject>(prefab_rollover, this.transform);
        roll_instance.transform.localPosition = Vector3.zero;
        SetupColorIsland();
        roll_instance.GetComponentInChildren<TMP_Text>().text = string.Format("{0}\nid: {1}", island_name, island_id);
        //roll_instance.GetComponentInChildren<TMP_Text>().text = string.Format("{0}", island_name);
        roll_instance.transform.localScale = new Vector3(0.2f / this.transform.lossyScale.x, 0.2f / this.transform.lossyScale.y, 0.2f / this.transform.lossyScale.z);
        //roll_instance.transform.localScale = Vector3.one;
        roll_instance.transform.SetParent(this.transform.parent);
        roll_instance.transform.SetAsLastSibling();
#endif
    }

    public void OnPointerExit(PointerEventData eventData) {
#if UNITY_EDITOR
        RemoveInstance();
#endif
    }

    /// <summary>
    /// Get the color side from the team id.
    /// </summary>
    /// <returns>The color of the island</returns>
    private Color GetColorSide() {
        switch(team_id) {
            default:
            case 0:
                return color_neutral;
                break;
            case 1:
                return color_epsilon;
                break;
            case 2:
                return color_omega;
                break;
        }
    }
    /// <summary>
    /// Set the color island from whom it belongs.
    /// </summary>
    private void SetupColorIsland() {
        Color col = GetColorSide();
        // not needed anymore.
        //roll_instance.GetComponentsInChildren<Image>().ToList<Image>().ForEach((n) => { n.color = col; });
        this.GetComponent<Image>().color = col;
    }

    

    /// <summary>
    /// Remove the rollover instance.
    /// </summary>
    private void RemoveInstance() {
        Destroy(roll_instance);
    }

    /// <summary>
    /// When clicking on the island, it opens a detail panel.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData) {
        /*team_id++;
        if (team_id > 2)
            team_id = 0;
        SetupColorIsland();
        // trigger the island changed event
        MainManager.IslandChanged();*/
        //Debug.Log(Time.time - T0);
        if (Time.time - T0 < 0.1f) {
            MapManager.instance.ShowIslandDetailsPanel(island_name, island_id, team_id);
            OnSelectIsland(island_id);
        }
    }

    private float T0;

    public void OnPointerDown(PointerEventData eventData) {
        T0 = Time.time;
        dragging = this;
    }

    public void OnPointerUp(PointerEventData eventData) {
        dragging = null;
    }

    private void Update() {
        if (dragging != null && dragging == this && Time.time-T0>0.1f) {
            // Dragging the island icon.
            Vector3 pos = new Vector3();
            RectTransformUtility.ScreenPointToWorldPointInRectangle(this.transform as RectTransform, Input.mousePosition, Camera.main, out pos);
            (this.transform as RectTransform).position = pos;
            MainManager.IslandChanged();
        }
    }

    /// <summary>
    /// Change island allegiance (triggered by an event)
    /// </summary>
    /// <param name="_island_id">the id of the island to change</param>
    /// <param name="_team_id">the new team id to set</param>
    private void SetAllegiance(int _island_id, int _team_id) {
        if (this.island_id != _island_id || _island_id < 0) return;
        team_id = _team_id;
        SetupColorIsland();
        // trigger the island changed event
        MainManager.IslandChanged();
    }

    /// <summary>
    /// Try to select the island.
    /// if the id sent by the event is right, 
    /// </summary>
    /// <param name="_island_id">id of the selected island.</param>
    private void SelectMe(int _island_id) {
        if (this.island_id == _island_id) {
            // Displays the target
            DisplayTarget();
        } else {
            // Hide the target, the island is not selected.
            HideTarget();
        }
    }

    /// <summary>
    /// Unselect the island (hide its target)
    /// </summary>
    private void UnselectMe() {
        HideTarget();
    }

    private void DisplayTarget() {
        Image img = target.GetComponent<Image>();
        img.color = new Color(img.color.r, img.color.g, img.color.b, 0f);
        target.SetActive(true);
        TweenAlpha.Tween(target, 0.5f, 1f);
    }
    private void HideTarget() {
        if (target.activeSelf)
            TweenAlpha.Tween(target, 0.4f, 0f, () => { target.SetActive(false); });
    }

}
