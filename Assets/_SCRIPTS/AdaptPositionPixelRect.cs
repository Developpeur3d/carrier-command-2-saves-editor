using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Convatec {
    /// <summary>
    /// Positionnement d'un RectTransform par rapport � un pixelRect d'une cam�ra.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [ExecuteInEditMode()]
    public class AdaptPositionPixelRect : MonoBehaviour {

        public Camera cam;
        public Vector2 offset = new Vector2(5f,-5f);
        private RectTransform rt;
        private Canvas canvas;

        void Start() {
            if (cam == null)
                cam = FindObjectsOfType<Camera>().Where(n => n.CompareTag("Cam cas")).FirstOrDefault();
            rt = this.transform as RectTransform;
            canvas = GetComponentInParent<Canvas>();
        }

        void Update() {
            if (cam == null) return;
            //rt.anchoredPosition = new Vector2(cam.pixelRect.x + 0f, (Screen.height - (cam.pixelRect.height + cam.pixelRect.y)) / canvas.scaleFactor - 5f);
            rt.anchoredPosition = new Vector2((cam.pixelRect.x+offset.x)/canvas.scaleFactor, (cam.pixelRect.height -Screen.height + cam.pixelRect.y +offset.y)/canvas.scaleFactor);
        }
    }
}
