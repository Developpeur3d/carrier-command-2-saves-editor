using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System;
using System.Text.RegularExpressions;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
#if UNITY_EDITOR
using UnityEngine.Assertions;
#endif
using System.Linq;
using System.Globalization;
using UnityEngine.Events;
using System.Threading;

public class MainManager:MonoBehaviour {

    [Header("Components")]
    public TMP_Dropdown component_saves_dropdown;
    public TMP_InputField component_input_currency;
    public TMP_InputField component_input_fuel;
    public TMP_InputField component_input_weight;
    public Toggle component_toggle_accomega;
    public CanvasGroup component_currency_fuel_accomega;
    public CanvasGroup component_buttons;
    public Button component_button_save;
    public GameObject component_ammo;
    public GameObject component_largemunitions;
    public GameObject component_turrets;
    public GameObject component_utility;
    public GameObject component_surfacechassis;
    public GameObject component_airchassis;
    public GameObject component_preloader;
    public GameObject component_customisation;
    public TMP_Dropdown dropdown_custom_color_acc_epsilon;
    public TMP_Dropdown dropdown_custom_color_acc_omega;
    public TMP_Dropdown dropdown_custom_color_neutral;
    public Image image_custom_color_acc_epsilon;
    public Image image_custom_color_acc_omega;
    public Image image_custom_color_neutral;
    public Toggle component_blueprint;
    public TMP_Text component_infos;
    public GameObject component_help;
    public Button button_help;
    public GameObject component_scroll;
    // Not yet in use...
    public GameObject component_repairStatus;
    public TMP_InputField input_ammo160_loaded;
    public TMP_InputField input_cruisemissiles_loaded;
    public TMP_InputField input_aamissiles01_loaded;

    [Header("Island components")]
    public Sprite sprite_smallmunitions;
    public Sprite sprite_utility;
    public Sprite sprite_largemunitions;
    public Sprite sprite_turrets;
    public Sprite sprite_surfacechassis;
    public Sprite sprite_airchassis;
    public Sprite sprite_warehouse;
    public Sprite sprite_barge;
    public Sprite sprite_fuel;
    public Sprite sprite_epsilon;

    [Header("Map")]
    public GameObject prefab_island;
    public Transform component_mapContainer;

    [Header("Datas")]
    private string path;
    private List<Save> saved_slots;
    /// <summary>
    /// The input field corresponding to the inventories datas.
    /// Keep the right order in the list !!!! Otherwise, the inventory will be saved in the wrong order.
    /// </summary>
    public List<TMP_InputField> inventory_nodes;
    /// <summary>
    /// The current xml document of the edited saved slot.
    /// </summary>
    private XmlDocument current_slot;
    /// <summary>
    /// The Carrier specific datas.
    /// </summary>
    private XmlDocument xmlCarrier;
    /// <summary>
    /// The Ammo 160mm (carrier canon) specific datas 
    /// </summary>
    private XmlDocument xmlAmmo160;
    /// <summary>
    /// The cruise missile specific datas
    /// </summary>
    private XmlDocument xmlCruiseMissile;
    private XmlDocument xmlAAMissile;
    private XmlDocument xmlAAMissile2;
    private CultureInfo ci;
    [HideInInspector()]
    public Color[] teams_colors;

    /// <summary>
    /// The islands names list (the names corresponding to their ids)
    /// DO NOT change it if you want the names to be accurate to the ones in the game...
    /// </summary>
    public List<Island> Islands_names;

    /// <summary>
    /// The infos displayed for each menu item.
    /// </summary>
    public string[] infos;

    #region events
    // =================== EVENTS =====================
    public delegate void OnIslandChanged();
    public static event OnIslandChanged EventOnIslandChanged;
    public static void IslandChanged() {
        if (EventOnIslandChanged != null)
            EventOnIslandChanged();
    }
    /// <summary>
    /// When application is resized...
    /// </summary>
    public delegate void ChangeResolution();
    public static event ChangeResolution EventChangeResolution;
    public static void OnChangeResolution() {
        if (EventChangeResolution != null) {
            EventChangeResolution();
        }
    }
    // ================================================
    #endregion


    void Start() {
        DisplayPreload();
#if UNITY_EDITOR
        Assert.IsNotNull(component_saves_dropdown);
        Assert.IsNotNull(component_input_currency);
        Assert.IsNotNull(component_input_fuel);
        Assert.IsNotNull(component_ammo);
        Assert.IsNotNull(component_largemunitions);
        Assert.IsNotNull(component_turrets);
        Assert.IsNotNull(component_utility);
        Assert.IsNotNull(component_surfacechassis);
        Assert.IsNotNull(component_airchassis);
        Assert.IsNotNull(component_preloader);
        Assert.IsNotNull(component_currency_fuel_accomega);
        Assert.IsNotNull(component_buttons);
        Assert.IsNotNull(component_button_save);
        Assert.IsNotNull(component_toggle_accomega);
        Assert.IsNotNull(component_blueprint);
        Assert.IsNotNull(component_customisation);
        Assert.IsNotNull(dropdown_custom_color_acc_epsilon);
        Assert.IsNotNull(dropdown_custom_color_acc_omega);
        Assert.IsNotNull(dropdown_custom_color_neutral);
        Assert.IsNotNull(image_custom_color_acc_epsilon);
        Assert.IsNotNull(image_custom_color_acc_omega);
        Assert.IsNotNull(image_custom_color_neutral);
        Assert.IsNotNull(prefab_island);
        Assert.IsNotNull(component_mapContainer);
        Assert.IsNotNull(sprite_smallmunitions);
        Assert.IsNotNull(sprite_utility);
        Assert.IsNotNull(sprite_largemunitions);
        Assert.IsNotNull(sprite_turrets);
        Assert.IsNotNull(sprite_surfacechassis);
        Assert.IsNotNull(sprite_airchassis);
        Assert.IsNotNull(sprite_warehouse);
        Assert.IsNotNull(sprite_barge);
        Assert.IsNotNull(sprite_fuel);
     //   Assert.IsNotNull(sprite_epsilon);
        Assert.IsNotNull(component_infos);
        Assert.IsNotNull(component_help);
        Assert.IsNotNull(button_help);
        Assert.IsNotNull(component_scroll);
        Assert.IsNotNull(input_ammo160_loaded);
        Assert.IsNotNull(input_cruisemissiles_loaded);
#endif
        Application.wantsToQuit += WantsToQuit;
        ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        ci.NumberFormat.NumberDecimalSeparator = ".";
        Regex reg = new Regex(@".*(AppData\/)");
        path = reg.Match(Application.persistentDataPath).Value;
        path = path+ "Roaming/Carrier Command 2";
        Debug.Log(path);
        StartCoroutine(IE_LoadSavesDatas());
        StartCoroutine(LaunchVersionControl());
    }

    private void OnEnable() {
        EventOnIslandChanged += IslandModified;
        EventChangeResolution += WindowResolutionChanged;
    }
    private void OnDisable() {
        EventOnIslandChanged -= IslandModified;
        EventChangeResolution -= WindowResolutionChanged;
    }

    private void Update() {
        if (lastScreenWidth != Screen.width || lastScreenHeight != Screen.height) {
            lastScreenWidth = Screen.width;
            lastScreenHeight = Screen.height;
            OnChangeResolution();
        }
    }
    private int lastScreenWidth;
    private int lastScreenHeight;


    public bool WantsToQuit() {
        if (component_button_save.interactable) {
            // You forgot to save changes...
            Notification.Confirm("Warning", "You forgot to save changes. Do you really want to quit ?", (n)=> {
                if (n)
                    component_button_save.interactable = false;
                Application.Quit();
            }, "YES", "NO");
            return false;
        } else {
            return true;
        }
    }

    #region Loading

    public void Refresh() {
        StartCoroutine(IE_LoadSavesDatas());
        component_infos.text = infos[0];
    }

    /// <summary>
    /// Load filled saved slots from the "persistent_data.xml" file
    /// </summary>
    /// <returns></returns>
    private IEnumerator IE_LoadSavesDatas() {
        ClickMe.DisableAll();
        component_infos.text = infos[0];
        component_button_save.interactable = false;
        // Loading Xml of saves inventory.
        UnityWebRequest www = UnityWebRequest.Get(@"file:///"+Path.Combine(path, "persistent_data.xml"));
        yield return www.SendWebRequest();
        if (!string.IsNullOrEmpty(www.error)) {
            // Error
            Debug.LogErrorFormat("Error loading saves persistent xml file (\"{1}\") : \n{0}", www.error, path + "/persistent_data.xml");
            Error("Error loading saves persistent xml file (\"{1}\")", ()=> { Debug.Log("quit"); Application.Quit(); }, www.error, path.Replace("\\", "/") + "/persistent_data.xml");
            yield break;
        }
        string content = www.downloadHandler.text;
        XmlDocument xml = new XmlDocument();
        XmlReader xmlReader = XmlReader.Create(new StringReader(content));
        xml.Load(xmlReader);
        current_slot = xml;
        XmlNode datas = xml.SelectSingleNode("data");
        Regex reg = new Regex(@"^(slot_)");
        saved_slots = new List<Save>();
        List<TMP_Dropdown.OptionData> options_datas = new List<TMP_Dropdown.OptionData>();
        foreach(XmlNode node in datas.ChildNodes) {
            if (reg.IsMatch(node.Name)) {
                if (node.Attributes["display_name"].Value == "") continue;
                Save s = new Save(int.Parse(node.Attributes["time"].Value), node.Attributes["display_name"].Value, node.Attributes["save_name"].Value);
                saved_slots.Add(s);
                TMP_Dropdown.OptionData o = new TMP_Dropdown.OptionData(node.Attributes["display_name"].Value);
                options_datas.Add(o);
            }
        }
        component_saves_dropdown.options = options_datas;
        if (saved_slots.Count > 0)
            NewSavedSlotSelected(0);
        component_saves_dropdown.value = 0;
    }

    /// <summary>
    /// Triggered when a saved slot is selected in the dropdown list filled dynamically.
    /// </summary>
    /// <param name="id">index of the saved slot</param>
    public void NewSavedSlotSelected(int id) {
        ClickMe.DisableAll();
        component_infos.text = infos[0];
        if (component_button_save.interactable) {
            Notification.Confirm("WARNING", "You probably forgot to save your changes. Are you sure you want to continue ?", (n) => {
                if (n) {
                    component_button_save.interactable = false;
                    NewSavedSlotSelected(id);
                }
            }, "YES", "NO");
        } else {
            // Loading the saved datas
            if (loadingSavedDatas != null)
                StopCoroutine(loadingSavedDatas);
            loadingSavedDatas = StartCoroutine(IE_LoadSavedSlot(saved_slots[id].save_name));
        }
    }
    private Coroutine loadingSavedDatas;


    /// <summary>
    /// Loading saved data slot
    /// </summary>
    /// <param name="folderName">The saved folder name</param>
    /// <returns></returns>
    private IEnumerator IE_LoadSavedSlot(string folderName) {
        component_button_save.interactable = false;
        component_infos.text = infos[0];
        loadingFinished = false;
        DisplayPreload();
        string _path = Path.Combine(path, "saved_games", folderName, "save.xml");
        UnityWebRequest www = UnityWebRequest.Get(@"file:///" + _path);
        yield return www.SendWebRequest();
        if (!string.IsNullOrEmpty(www.error)) {
            // Error
            Debug.LogErrorFormat("Error loading saves xml file (\"{1}\") : \n{0}", www.error, _path);
            Error("Error loading saves xml file (\"{1}\")", () => { component_saves_dropdown.value = 0; HidePreload(); }, www.error, _path.Replace("\\", "/"));
            yield break;
        }
        try {
            string content = www.downloadHandler.text;
            // The xml file is malformed (stricto sensu). So we're adding a wrapper around it to be considered as a "valid" xml stream
            // and being able to manipulate it.
            string[] lines = content.Split(Environment.NewLine.ToCharArray()).Skip(1).ToArray();
            content = string.Join(Environment.NewLine, lines);
            content = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>\n<data>\n" + content + "\n</data>";
            //Debug.Log(content);

            XmlDocument xml = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = false;
            XmlReader xmlReader = XmlReader.Create(new StringReader(content), settings);
            current_slot = xml;
            xml.Load(xmlReader);
            XmlNode v_states = xml.SelectSingleNode("data/vehicles/vehicle_states");
            XmlNode carrier = v_states.SelectNodes("v[@id='2']")?[0];
            if (carrier == null) {
                Debug.LogError("No carrier node !!!");
                yield break;
            }
            // Currency
            component_input_currency.text = xml.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false']").Attributes["currency"].Value;
            // ACC Omega destroyed
            component_toggle_accomega.isOn = xml.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='false']").Attributes["is_destroyed"].Value == "true";
            // Blueprints
            int val = 0;
            component_blueprint.interactable = true;
            foreach (XmlNode node in xml.SelectNodes("data/scene/teams/teams/t[@is_ai_controlled='false']/unlocked_blueprints/bytes/b")) {
                val += int.Parse(node.Attributes["value"].Value);
            }
            component_blueprint.isOn = xml.SelectNodes("data/scene/teams/teams/t[@is_ai_controlled='false']/unlocked_blueprints/bytes/b").Count == 7 && val == 255 * 7;
            if (component_blueprint.isOn)
                component_blueprint.interactable = false;

            // Loaded inventory
            // 160mm canon
            string state01 = carrier.SelectSingleNode("attachments/a[@attachment_index='7']").Attributes["state"].Value;
            xmlAmmo160 = new XmlDocument();
            XmlReader xmlReaderAmmo160 = XmlReader.Create(new StringReader(state01), settings);
            xmlAmmo160.Load(xmlReaderAmmo160);
            input_ammo160_loaded.text = xmlAmmo160.SelectSingleNode("data").Attributes["ammo"].Value;

            // Cruise missiles
            string state02 = carrier.SelectSingleNode("attachments/a[@attachment_index='8']").Attributes["state"].Value;
            xmlCruiseMissile = new XmlDocument();
            XmlReader xmlReaderCruiseMissile = XmlReader.Create(new StringReader(state02), settings);
            xmlCruiseMissile.Load(xmlReaderCruiseMissile);
            input_cruisemissiles_loaded.text = xmlCruiseMissile.SelectSingleNode("data").Attributes["ammo"].Value;

            // AA missiles
            string state03a = carrier.SelectSingleNode("attachments/a[@attachment_index='5']").Attributes["state"].Value;
            xmlAAMissile= new XmlDocument();
            XmlReader xmlReaderAAmissiles1 = XmlReader.Create(new StringReader(state03a), settings);
            xmlAAMissile.Load(xmlReaderAAmissiles1);
            input_aamissiles01_loaded.text = xmlAAMissile.SelectSingleNode("data").Attributes["ammo"].Value;

            // Loading Carrier's datas
            string state = carrier.Attributes["state"].Value;
            xmlCarrier = new XmlDocument();
            XmlReader xmlReaderCarrier = XmlReader.Create(new StringReader(state), settings);
            xmlCarrier.Load(xmlReaderCarrier);

            // Fuel remaining
            float fuelValue = float.Parse(xmlCarrier.SelectSingleNode("data").Attributes["internal_fuel_remaining"].Value, ci);
            component_input_fuel.text = fuelValue.ToString();
            // Total weight
            float totalWeight = float.Parse(xmlCarrier.SelectSingleNode("data/inventory").Attributes["total_weight"].Value.Replace(",", "."), ci);
            component_input_weight.text = totalWeight.ToString();

            #region Inventory
            for (int i = 0; i < inventory_nodes.Count; i++) {
                try {
                    inventory_nodes[i].text = xmlCarrier.SelectSingleNode("data/inventory/item_quantities").SelectNodes("q")[i].Attributes["value"].Value;
                } catch {
                    continue;
                }
            }
            #endregion

            // Custom colors
            dropdown_custom_color_acc_epsilon.value = int.Parse(xml.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false' and @is_neutral='false']").Attributes["pattern_index"].Value)-1;
            dropdown_custom_color_acc_omega.value = int.Parse(xml.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='false']").Attributes["pattern_index"].Value)-1;
            dropdown_custom_color_neutral.value = int.Parse(xml.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='true']").Attributes["pattern_index"].Value)-1;

            //Debug.Log(state);
            //Debug.Log(RemoveDataNode(current_slot));

            // Draw map islands
            DrawMapIslands();

            HidePreload();
            loadingFinished = true;
        } catch(Exception e) {
            Debug.LogError(e.Message);
            Notification.Alert("Error", "An error occured during the loading of this slot. Maybe a multiplayer or a corrupted save ?");
            HidePreload();
        }
        HideIslandDetails();
        yield break;
    }

    #endregion

    #region Save process

    /// <summary>
    /// Save all datas to the current opened save slot file.
    /// The saving process is preceeded by a backup (we never know !)
    /// </summary>
    public void SaveAllMyDatas() {
        // disable all menu buttons
        ClickMe.DisableAll();
        component_infos.text = infos[0];
        // display preload.
        DisplayPreload();
        // Create a backup...
        string sourceFile = Path.Combine(path, "saved_games", saved_slots[component_saves_dropdown.value].save_name, "save.xml");
        string destinationFile = Path.Combine(path, "saved_games", saved_slots[component_saves_dropdown.value].save_name, "save_backup.xml");
        if (!File.Exists(destinationFile)) {
            try {
                File.Copy(sourceFile, destinationFile, true);
            } catch (IOException iox) {
                Debug.LogError(iox.Message);
                Error("Error making backup !<br>I won't save your changes... This is not safe.", () => {
                    HidePreload();
                });
                return;
            }
        }

        // currency
        current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false']").Attributes["currency"].Value = component_input_currency.text;
        // acc omega
        current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='false']").Attributes["is_destroyed"].Value = component_toggle_accomega.isOn ? "true" : "false";
        // fuel
        xmlCarrier.SelectSingleNode("data").Attributes["internal_fuel_remaining"].Value = string.Format("{0:0.######e+00}", float.Parse(component_input_fuel.text)).Replace(",",".");
        // total weight
        xmlCarrier.SelectSingleNode("data/inventory").Attributes["total_weight"].Value = string.Format("{0:0.######e+00}", float.Parse(component_input_weight.text)).Replace(",", ".");
        // blueprints
        if (component_blueprint.isOn && component_blueprint.interactable) {
            current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false']/unlocked_blueprints/bytes").RemoveAll();
            XmlAttribute a = current_slot.CreateAttribute("size");
            current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false']/unlocked_blueprints/bytes").Attributes.Append(a).Value = "7";
            for(int i=0;i<7; i++) {
                XmlNode node = current_slot.CreateNode("element", "b", "");
                XmlAttribute attr = current_slot.CreateAttribute("value");
                node.Attributes.Append(attr).Value = "255";
                current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false']/unlocked_blueprints/bytes").AppendChild(node);
            }
        }
        component_blueprint.interactable = false;
        // all islands
        foreach (Transform child in component_mapContainer.transform) {
            current_slot.SelectSingleNode("data/scene/tiles/tiles/t[@index='" + child.GetComponent<IslandRollover>().island_id + "']").Attributes["team_control"].Value = child.GetComponent<IslandRollover>().team_id.ToString();
            current_slot.SelectSingleNode("data/scene/tiles/tiles/t[@index='" + child.GetComponent<IslandRollover>().island_id + "']/world_position").Attributes["x"].Value = (((child as RectTransform).anchoredPosition.x + 100f) * 180000f/200f).ToString("0.##########e+00").Replace(",", ".");
            current_slot.SelectSingleNode("data/scene/tiles/tiles/t[@index='" + child.GetComponent<IslandRollover>().island_id + "']/world_position").Attributes["z"].Value = (((child as RectTransform).anchoredPosition.y + 100f) * 150000f / 200f).ToString().Replace(",", ".");
        }

            // all inventory assets
            for (int i=0; i<inventory_nodes.Count; i++) {
            try {
                xmlCarrier.SelectSingleNode("data/inventory/item_quantities").SelectNodes("q")[i].Attributes["value"].Value = inventory_nodes[i].text;
                // DEBUG to find inventory index:
                //xmlCarrier.SelectSingleNode("data/inventory/item_quantities").SelectNodes("q")[i].Attributes["value"].Value = i.ToString();
            } catch {
                try {
                    xmlCarrier.SelectSingleNode("data/inventory/item_quantities").SelectNodes("q")[i].Attributes["value"].Value = "0";
                } catch {
                    // Old save from an old version...
                }
                continue;
            }
        }

        // Ammo 160mm datas (carrier's gun)
        xmlAmmo160.SelectSingleNode("data").Attributes["ammo"].Value = input_ammo160_loaded.text;
        XmlDocument saveAmmo160Xml = new XmlDocument();
        XmlDocumentFragment docAmmo160Fragment = saveAmmo160Xml.CreateDocumentFragment();
        docAmmo160Fragment.InnerXml = xmlAmmo160.OuterXml;
        XmlWriterSettings writerSettings = new XmlWriterSettings();
        writerSettings.Indent = true;
        writerSettings.ConformanceLevel = ConformanceLevel.Fragment;
        writerSettings.DoNotEscapeUriAttributes = true;
        writerSettings.NewLineOnAttributes = false;
        StringWriter stringAmmo160Writer = new StringWriter();
        using (XmlWriter xmlAmmo160Writer = XmlWriter.Create(stringAmmo160Writer, writerSettings)) {
            docAmmo160Fragment.WriteTo(xmlAmmo160Writer);
        }
        current_slot.SelectSingleNode("data/vehicles/vehicle_states/v[@id='2']/attachments/a[@attachment_index = '7']").Attributes["state"].Value = stringAmmo160Writer.ToString();

        // Cruise missile datas
        xmlCruiseMissile.SelectSingleNode("data").Attributes["ammo"].Value = input_cruisemissiles_loaded.text;
        XmlDocument saveCruiseMissileXml = new XmlDocument();
        XmlDocumentFragment docCruiseMissileFragment = saveCruiseMissileXml.CreateDocumentFragment();
        docCruiseMissileFragment.InnerXml = xmlCruiseMissile.OuterXml;
        StringWriter stringCruiseMissileWriter = new StringWriter();
        using (XmlWriter xmlCruiseMissileWriter = XmlWriter.Create(stringCruiseMissileWriter, writerSettings)) {
            docCruiseMissileFragment.WriteTo(xmlCruiseMissileWriter);
        }
        current_slot.SelectSingleNode("data/vehicles/vehicle_states/v[@id='2']/attachments/a[@attachment_index = '8']").Attributes["state"].Value = stringCruiseMissileWriter.ToString();

        // AA loaded missiles datas
        xmlAAMissile.SelectSingleNode("data").Attributes["ammo"].Value = input_aamissiles01_loaded.text;
        XmlDocument saveAAMissileXml = new XmlDocument();
        XmlDocumentFragment docAAMissileFragment = saveAAMissileXml.CreateDocumentFragment();
        docAAMissileFragment.InnerXml = xmlCruiseMissile.OuterXml;
        StringWriter stringAAMissileWriter = new StringWriter();
        using (XmlWriter xmlCruiseMissileWriter = XmlWriter.Create(stringAAMissileWriter, writerSettings)) {
            docCruiseMissileFragment.WriteTo(xmlCruiseMissileWriter);
        }
        current_slot.SelectSingleNode("data/vehicles/vehicle_states/v[@id='2']/attachments/a[@attachment_index = '5']").Attributes["state"].Value = stringAAMissileWriter.ToString();
        current_slot.SelectSingleNode("data/vehicles/vehicle_states/v[@id='2']/attachments/a[@attachment_index = '6']").Attributes["state"].Value = stringAAMissileWriter.ToString();

        // Custom colors
        current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='false' and @is_neutral='false']").Attributes["pattern_index"].Value = (dropdown_custom_color_acc_epsilon.value + 1).ToString();
        current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='false']").Attributes["pattern_index"].Value = (dropdown_custom_color_acc_omega.value + 1).ToString();
        current_slot.SelectSingleNode("data/scene/teams/teams/t[@is_ai_controlled='true' and @is_neutral='true']").Attributes["pattern_index"].Value = (dropdown_custom_color_neutral.value + 1).ToString();

        // Carrier's datas
        XmlDocument saveCarrierXml = new XmlDocument();
        XmlDocumentFragment docCarrierFragment = saveCarrierXml.CreateDocumentFragment();
        docCarrierFragment.InnerXml = xmlCarrier.OuterXml;
        StringWriter stringCarrierWriter = new StringWriter();
        using (XmlWriter xmCarrierWriter = XmlWriter.Create(stringCarrierWriter, writerSettings)) {
            docCarrierFragment.WriteTo(xmCarrierWriter);
        }
        //Debug.Log(stringCarrierWriter.ToString());
        current_slot.SelectSingleNode("data/vehicles/vehicle_states").SelectNodes("v[@id='2']")[0].Attributes["state"].Value = stringCarrierWriter.ToString();


        // Agregates datas
        XmlDocument saveXml = new XmlDocument();
        XmlDocumentFragment docFragment = saveXml.CreateDocumentFragment();
        docFragment.InnerXml = current_slot.OuterXml;
        StringWriter stringWriter = new StringWriter();
        using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings)) {
            docFragment.WriteTo(xmlWriter);
        }

        //Debug.Log(RemoveDataNode(stringWriter.ToString()));
        // 
        File.WriteAllText(sourceFile, RemoveDataNode(stringWriter.ToString()));
        HidePreload();
        component_button_save.interactable = false;

        HideIslandDetails();
        Notification.Alert("YOUR SAVE IS MODIFIED", "Saved succesfully.\n\nFor info, a backup of the original file is available at <i>" + destinationFile.Replace("\\", "/") + "</i>");
    }

    /// <summary>
    /// Export and format Xml string to be embedded in a xml node.
    /// </summary>
    /// <param name="_xml">the original xml stream</param>
    /// <returns>the formated xml string</returns>
    private string ExportXmlString(string _xml) {
        string xml = _xml.Replace("<", "&lt;");
        xml = xml.Replace(">", "&gt;");
        return xml;
    }

    private string ImportXmlString(string _xml) {
        string xml = _xml.Replace("&lt;", "<");
        xml = xml.Replace("&gt;", ">");
        return xml;
    }

    private string RemoveDataNode(string _string) {
        string tempXmlString = "";
        tempXmlString = _string.Replace("<data>"+ Environment.NewLine, "");
        tempXmlString = tempXmlString.Remove(tempXmlString.TrimEnd().LastIndexOf(Environment.NewLine));
        return tempXmlString;
    }
    
    /// <summary>
    /// When a value is changed, the save button is activated.
    /// </summary>
    public void ValueChanged() {
        if (loadingFinished)
            component_button_save.interactable = true;
    }
    private bool loadingFinished;
#endregion

    #region UI management

    /// <summary>
    /// Select an inventory menu (and displays it)
    /// </summary>
    /// <param name="id">inventory id</param>
    public void SelectInventory(int id) {
        try {
            MapManager.instance.HideIslandDetailsPanel();
        } catch { }
        StartCoroutine(IE_SelectInventory(id));
    }

    private IEnumerator IE_SelectInventory(int id) {
        HideAll();
        ClickMe.DisableAll();
        yield return new WaitForSeconds(tween_time);
        switch (id) {
            default:
            case 0:
                // AMMOS
                component_scroll.SetActive(true);
                component_ammo.SetActive(true);
                TweenCGAlpha.Tween(component_ammo, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 1:
                // LARGE MUNITIONS
                component_scroll.SetActive(true);
                component_largemunitions.SetActive(true);
                TweenCGAlpha.Tween(component_largemunitions, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 2:
                // TURRETS
                component_scroll.SetActive(true);
                component_turrets.SetActive(true);
                TweenCGAlpha.Tween(component_turrets, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 3:
                // UTILITY
                component_scroll.SetActive(true);
                component_utility.SetActive(true);
                TweenCGAlpha.Tween(component_utility, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 4:
                // SURFACE CHASSIS
                component_scroll.SetActive(true);
                component_surfacechassis.SetActive(true);
                TweenCGAlpha.Tween(component_surfacechassis, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 5:
                // AIR CHASSIS
                component_scroll.SetActive(true);
                component_airchassis.SetActive(true);
                TweenCGAlpha.Tween(component_airchassis, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
            case 6:
                // MAP EDITOR
                component_scroll.SetActive(false);
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                DrawMapIslands();
                component_mapContainer.parent.parent.gameObject.SetActive(true);
                TweenCGAlpha.Tween(component_mapContainer.parent.parent.gameObject, 0.5f, 1f);
                component_infos.text = infos[1];
                break;
            case 7:
                // CUSTOMISATION
                component_scroll.SetActive(true);
                component_customisation.SetActive(true);
                TweenCGAlpha.Tween(component_customisation, 0.5f, 1f);
                component_infos.text = infos[2];
                break;
            case 8:
                // REPAIR STATUS
                component_scroll.SetActive(true);
                component_repairStatus.SetActive(true);
                TweenCGAlpha.Tween(component_repairStatus, 0.5f, 1f);
                component_infos.text = infos[0];
                break;
        }
        yield break;
    }
    /// <summary>
    /// the tween time to disapear (in seconds)
    /// </summary>
    private const float tween_time = 0.3f;
    /// <summary>
    /// Hide all inventory contents.
    /// </summary>
    private void HideAll() {
        if (component_ammo.activeSelf)
            TweenCGAlpha.Tween(component_ammo, tween_time, 0f, () => {
                component_ammo.SetActive(false);
            });
        if (component_largemunitions.activeSelf)
            TweenCGAlpha.Tween(component_largemunitions, tween_time, 0f, () => {
                component_largemunitions.SetActive(false);
            });
        if (component_turrets.activeSelf)
            TweenCGAlpha.Tween(component_turrets, tween_time, 0f, () => {
                component_turrets.SetActive(false);
            });
        if (component_utility.activeSelf)
            TweenCGAlpha.Tween(component_utility, tween_time, 0f, () => {
                component_utility.SetActive(false);
            });
        if (component_surfacechassis.activeSelf)
            TweenCGAlpha.Tween(component_surfacechassis, tween_time, 0f, () => {
                component_surfacechassis.SetActive(false);
            });
        if (component_airchassis.activeSelf)
            TweenCGAlpha.Tween(component_airchassis, tween_time, 0f, () => {
                component_airchassis.SetActive(false);
            });
        if (component_mapContainer.parent.parent.gameObject.activeSelf)
            TweenCGAlpha.Tween(component_mapContainer.parent.parent.gameObject, tween_time, 0f, () => {
                component_mapContainer.parent.parent.gameObject.SetActive(false);
            });
        if (component_customisation.activeSelf)
            TweenCGAlpha.Tween(component_customisation, tween_time, 0f, () => {
                component_customisation.SetActive(false);
            });
    }

    /// <summary>
    /// Display the preload component
    /// </summary>
    public void DisplayPreload() {
        component_currency_fuel_accomega.alpha = 0.5f;
        component_currency_fuel_accomega.interactable = false;
        component_buttons.interactable = false;
        HideAll();
        component_preloader.GetComponent<CanvasGroup>().alpha = 0f;
        component_preloader.SetActive(true);
        TweenCGAlpha.Tween(component_preloader, 0.5f, 1f);
    }

    /// <summary>
    /// Hide the preload component.
    /// </summary>
    public void HidePreload() {
        component_currency_fuel_accomega.alpha = 1f;
        component_currency_fuel_accomega.interactable = true;
        component_buttons.interactable = true;
        TweenCGAlpha.Tween(component_preloader, 0.5f, 0f, () => { component_preloader.SetActive(false); });
    }

    public void ChangeAccOmega(bool val) {
        if (val) {
            // Acc Omega is destroyed
            component_toggle_accomega.transform.parent.GetComponentInChildren<TMP_Text>().color = Color.red;
        } else {
            // Acc Omega is not destroyed
            component_toggle_accomega.transform.parent.GetComponentInChildren<TMP_Text>().color = Color.black;
        }
    }
    private Coroutine coroutine_help;




    #endregion

    #region Custom colors
    /// <summary>
    /// When the user selects a new color for the acc epsilon.
    /// </summary>
    /// <param name="val">color index value</param>
    public void SelectCustomColor_AccEpsilon(int val) {
        // we are verifing the colors between each camp. If a color is the same from one another, we're raising an alert.
        if (VerifyColor(0)) {
            // This color is already assigned. We're throwing a notification.
            Notification.Alert("Warning", "This color is already assigned to another team.");
            dropdown_custom_color_acc_epsilon.SetValueWithoutNotify(lastColor_AccEpsilon);
            image_custom_color_acc_epsilon.sprite = dropdown_custom_color_acc_epsilon.options[lastColor_AccEpsilon].image;
        } else {
            // The color is free to be used
            //IslandRollover.color_epsilon = teams_colors[val];
            lastColor_AccEpsilon = val;
            image_custom_color_acc_epsilon.sprite = dropdown_custom_color_acc_epsilon.options[val].image;
        }
    }
    private int lastColor_AccEpsilon;

    /// <summary>
    /// When the user selects a new color for the acc omega.
    /// </summary>
    /// <param name="val">color index value</param>
    public void SelectCustomColor_AccOmega(int val) {
        // we are verifing the colors between each camp. If a color is the same from one another, we're raising an alert.
        if (VerifyColor(2)) {
            // This color is already assigned. We're throwing a notification.
            Notification.Alert("Warning", "This color is already assigned to another team.");
            dropdown_custom_color_acc_omega.SetValueWithoutNotify(lastColor_AccOmega);
            image_custom_color_acc_omega.sprite = dropdown_custom_color_acc_omega.options[lastColor_AccOmega].image;
        } else {
            // The color is free to be used
            //IslandRollover.color_omega = teams_colors[val];
            lastColor_AccOmega = val;
            image_custom_color_acc_omega.sprite = dropdown_custom_color_acc_omega.options[val].image;
        }
    }
    private int lastColor_AccOmega;

    /// <summary>
    /// When the user selects a new color for the neutral camp.
    /// </summary>
    /// <param name="val">color index value</param>
    public void SelectCustomColor_Neutral(int val) {
        // we are verifing the colors between each camp. If a color is the same from one another, we're raising an alert.
        if (VerifyColor(1)) {
            // This color is already assigned. We're throwing a notification.
            Notification.Alert("Warning", "This color is already assigned to another team.");
            dropdown_custom_color_neutral.SetValueWithoutNotify(lastColor_Neutral);
            image_custom_color_neutral.sprite = dropdown_custom_color_neutral.options[lastColor_Neutral].image;
        } else {
            // The color is free to be used
            //IslandRollover.color_neutral = teams_colors[val];
            lastColor_Neutral = val;
            image_custom_color_neutral.sprite = dropdown_custom_color_neutral.options[val].image;
        }
    }
    private int lastColor_Neutral;

    /// <summary>
    /// Verifiy if a color is already taken by a camp.
    /// </summary>
    /// <param name="side">the side we're checking</param>
    /// <returns>true if already taken</returns>
    private bool VerifyColor(int side) {
        switch (side) {
            case 0:
            default:
                // Acc Epsilon
                return dropdown_custom_color_acc_epsilon.value == dropdown_custom_color_acc_omega.value || dropdown_custom_color_acc_epsilon.value == dropdown_custom_color_neutral.value;
                break;
            case 1:
                // Neutral
                return dropdown_custom_color_neutral.value == dropdown_custom_color_acc_epsilon.value || dropdown_custom_color_neutral.value == dropdown_custom_color_acc_omega.value;
                break;
            case 2:
                // Acc Omega
                return dropdown_custom_color_acc_omega.value == dropdown_custom_color_neutral.value || dropdown_custom_color_acc_omega.value == dropdown_custom_color_acc_epsilon.value;
                break;
        }
    }
    #endregion

    #region Help
    /// <summary>
    /// display infos panel.
    /// </summary>
    public void Help() {
        button_help.enabled = false;
        if (coroutine_help != null) {
            StopCoroutine(coroutine_help);
        }
        coroutine_help = StartCoroutine(IE_Help());
    }
    private IEnumerator IE_Help() {
        HideIslandDetails();
        // checking latest version
        if (checkvUpdate != null) {
            StopCoroutine(checkvUpdate);
        }
        checkvUpdate = StartCoroutine(CheckVersionUpdate());
        yield return checkvUpdate;
        component_help.GetComponent<CanvasGroup>().alpha = 0f;
        component_help.SetActive(true);
        TweenCGAlpha.Tween(component_help, 0.5f, 1f);
        button_help.enabled = true;
        yield break;
    }
    /// <summary>
    /// Hide info panel
    /// </summary>
    public void CloseHelp() {
        TweenCGAlpha.Tween(component_help, 0.5f, 0f, () => component_help.SetActive(false));
    }

    private Coroutine checkvUpdate;
    private static Color redButton = new Color(255f / 255f, 157f / 255f, 157f / 255f);
    private IEnumerator CheckVersionUpdate() {
        UnityWebRequest www = UnityWebRequest.Get(@"http://www.developpeur3d.com/downloads/cc2saveseditor/cc2.xml");
        yield return www.SendWebRequest();
        if (!string.IsNullOrEmpty(www.error)) {
            // Error loading latest version xml file.
            Debug.LogError("Error loading latest version...");
            yield break;
        }
        string source = www.downloadHandler.text;
        XmlDocument xml = new XmlDocument();
        XmlReader xmlReader = XmlReader.Create(new StringReader(source));
        xml.Load(xmlReader);
        XmlNode datas = xml.SelectSingleNode("data");
        float latest_version = float.Parse(datas.SelectSingleNode("latest_version").InnerText, ci);
        string path = datas.SelectSingleNode("path").InnerText;

        // version number
        float version = float.Parse(Application.version, ci);
        TMP_Text t = component_help.GetComponentInChildren<TMP_Text>();
        string update_available = "";
        if (latest_version > version) {
            // A new update is available
            update_available = "<color=orange><b>You're version is <u>not</u> up to date.<br><size=40><u><link=" + path + ">v" + latest_version + " is available</link></u></size></b></color>";
            // Blinking help button
            if (button_help.GetComponent<TweenBtnColor>() == null) {
                TweenColor.Tween(button_help.gameObject, 1f, Color.white, redButton, TweenMain.Style.PingPong, TweenMain.Method.Linear);
            }
        } else {
            // You're up to date
            update_available = "<color=green><b>You're version is up to date.</b></color>";
        }
        t.text = t.text.Replace("{0}", version.ToString().Replace(",", ".")).Replace("{1}", update_available);
    }

    /// <summary>
    /// Recursive method to check regularly if an update is available.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LaunchVersionControl() {
        yield return StartCoroutine(CheckVersionUpdate());
        // Every 10mn
        yield return new WaitForSeconds(60f * 10f);
        StartCoroutine(LaunchVersionControl());
    }

    #endregion

    #region Island

    private void DrawMapIslands() {
        //Debug.Log("DrawMapIslands");
        StartCoroutine(IE_DrawMapIsland());
    }
    private IEnumerator IE_DrawMapIsland() {
        if (!component_mapContainer.gameObject.activeSelf)
            yield break;
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        component_mapContainer.transform.localPosition = Vector3.zero;
        foreach (Transform child in component_mapContainer.transform)
            GameObject.Destroy(child.gameObject);
        UpdateIslandSize();
        foreach (XmlNode node in current_slot.SelectNodes("data/scene/tiles/tiles/t")) {
            var x = float.Parse(node.SelectSingleNode("world_position").Attributes["x"].Value, ci) * 200f / 180000f - 100f;
            var y = float.Parse(node.SelectSingleNode("world_position").Attributes["z"].Value, ci) * 200f / 150000f - 100f;
            /*if (node.Attributes["index"].Value == "39") {
            }*/
            GameObject g = Instantiate<GameObject>(prefab_island);
            g.transform.SetParent(component_mapContainer);
            g.transform.localPosition = Vector3.zero;
            (g.transform as RectTransform).anchoredPosition = new Vector2(x, y);
            g.transform.localScale = new Vector3(minScale, minScale, minScale);
            switch (int.Parse(node.Attributes["team_control"].Value)) {
                default:
                case 0:
                    // neutral
                    //g.GetComponent<Image>().color = teams_colors[dropdown_custom_color_neutral.value];
                    g.GetComponent<Image>().color = IslandRollover.color_neutral;
                    break;
                case 1:
                    // player's island
                    //g.GetComponent<Image>().color = teams_colors[dropdown_custom_color_acc_epsilon.value];
                    g.GetComponent<Image>().color = IslandRollover.color_epsilon;
                    break;
                case 2:
                    // ennemi's island
                    //g.GetComponent<Image>().color = teams_colors[dropdown_custom_color_acc_omega.value];
                    g.GetComponent<Image>().color = IslandRollover.color_omega;
                    break;
            }
            IslandRollover ir = g.GetComponent<IslandRollover>();
            ir.team_id = int.Parse(node.Attributes["team_control"].Value);
            ir.island_id = int.Parse(node.Attributes["index"].Value);
            ir.island_type = int.Parse(node.SelectSingleNode("facility").Attributes["category"].Value);
            ir.island_name = GetIslandName(ir.island_id).ToUpper();
            g.GetComponentInChildren<TMP_Text>().text = ir.island_name;
            g.GetComponentInChildren<Image>().sprite = GetSpriteIsland(ir.island_type);
            g.GetComponentInChildren<AspectRatioFitter>().aspectRatio = (float)g.GetComponentInChildren<Image>().sprite.texture.width / (float)g.GetComponentInChildren<Image>().sprite.texture.height;
        }
    }

    private void UpdateIslandSize() {
        try {
            RectTransform r = (component_mapContainer.parent.parent as RectTransform);
            float height = r.rect.height;
            float width = r.rect.width;
            minScale = height / 250f;
            (component_mapContainer.parent as RectTransform).sizeDelta = new Vector2(width, height);
            (component_mapContainer.parent as RectTransform).anchoredPosition = new Vector2(width * 0.5f, -height * 0.5f);
            component_mapContainer.transform.localScale = new Vector3(minScale, minScale, minScale);
            component_mapContainer.GetComponent<MapManager>().minScale = minScale;
            component_mapContainer.GetComponent<MapManager>().maxScale = minScale * 6f;
            minScale = Mathf.Min(minScale, 2f);
        } catch { }
    }
    private float minScale;

    private string GetIslandName(int _id) {
        return Islands_names.Where<Island>(n => n.id == _id).FirstOrDefault()?.name;
    }

    /// <summary>
    /// Get sprite of the island from its type.
    /// </summary>
    private Sprite GetSpriteIsland(int _type_id) {
        switch (_type_id) {
            default:
            case 0:
                // Warehouse island
                return sprite_warehouse;
                break;
            case 1:
                // Small munitions island
                return sprite_smallmunitions;
                break;
            case 2:
                // Large munitions island
                return sprite_largemunitions;
                break;
            case 3:
                // Turrets island
                return sprite_turrets;
                break;
            case 4:
                // Utility island
                return sprite_utility;
                break;
            case 5:
                // Surface Chassis island
                return sprite_surfacechassis;
                break;
            case 6:
                // Air chassis island
                return sprite_airchassis;
                break;
            case 7:
                // Fuel island
                return sprite_fuel;
                break;
            case 8:
                // Barge island
                return sprite_barge;
                break;
        }
    }

    private void IslandModified() {
        component_button_save.interactable = true;
    }

    private void HideIslandDetails() {
        try {
            MapManager.instance.HideIslandDetailsPanel();
        } catch {
            if (MapManager.instance != null && MapManager.instance.IslandDetailsPanel.activeSelf)
                TweenCGAlpha.Tween(MapManager.instance.IslandDetailsPanel, 0.5f, 1f, () => {
                    MapManager.instance.IslandDetailsPanel.SetActive(false);
                });
        }
    }

    /// <summary>
    /// The application has changed its resolution.
    /// If map if displayed, we are redrawing it to fit the new resolution.
    /// </summary>
    private void WindowResolutionChanged() {
        DrawMapIslands();
    }
    #endregion

    #region Error management
    private void Error(string _string, UnityAction callback, params string[] val) {
        string returnedString = _string;
        for(int i=0; i<val.Count(); i++) {
            returnedString = returnedString.Replace("{" + i + "}", val[i]);
        }
        Notification.Alert("ERROR", returnedString, callback);
    }
#endregion
}

[Serializable]
public class Save {
    public int time;
    public string display_name;
    public string save_name;
    public Save(int _time, string _display_name, string _save_name) {
        time = _time;
        display_name = _display_name;
        save_name = _save_name;
    }
}

[Serializable]
public class Island {
    public int id;
    public string name;
}
