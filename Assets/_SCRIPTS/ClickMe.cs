using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Click event is thrown on the toggle button to trigger a menu method
/// </summary>

public class ClickMe:MonoBehaviour, IPointerClickHandler {

    public UnityEvent OnClick;

    void Start() {
    }

    public void OnPointerClick(PointerEventData eventData) {
        OnClick.Invoke();
        UncolorAll();
        this.GetComponent<Image>().color = this.GetComponent<Toggle>().colors.selectedColor;
    }

    /// <summary>
    /// Disable selected color on all toggle buttons.
    /// </summary>
    private void UncolorAll() {
        foreach(Image i in this.GetComponent<Toggle>().group.GetComponentsInChildren<Image>()) {
            i.color = i.GetComponent<Toggle>().colors.normalColor;
        }
    }

    public static void DisableAll() {
        foreach(ClickMe c in GameObject.FindObjectsOfType<ClickMe>()) {
            c.UncolorAll();
        }
    }
}