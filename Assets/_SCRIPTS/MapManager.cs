using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class MapManager:MonoBehaviour {

    /// <summary>
    /// Maximum scale to apply to the map (depends on the application size).
    /// </summary>
   // [HideInInspector()]
    public float maxScale = 1f;
    /// <summary>
    /// Minimum scale to apply on the map (depends on the application size);
    /// </summary>
  //  [HideInInspector()]
    public float minScale = 1f;

    private const float zoomFactor = 0.1f;

    public static MapManager instance;

    /// <summary>
    /// ths island details panel.
    /// </summary>
    public GameObject IslandDetailsPanel;

    void Start() {
        instance = this;
    }


    void Update() {
        //if (RectTransformUtility.RectangleContainsScreenPoint(this.transform as RectTransform, Input.mousePosition)) {
            if (Input.mouseScrollDelta.y != 0) {
                this.transform.localScale += new Vector3(zoomFactor, zoomFactor, zoomFactor) * Input.mouseScrollDelta.y;
                if (this.transform.localScale.x > maxScale)
                    this.transform.localScale = new Vector3(maxScale, maxScale, maxScale);
                if (this.transform.localScale.x < minScale)
                    this.transform.localScale = new Vector3(minScale, minScale, minScale);
            }

            if(Input.GetMouseButtonDown(0)) {
                clicked_point = Input.mousePosition;
                anchored_position = (this.transform as RectTransform).anchoredPosition;
                clicked = true;
                T0 = Time.time;
            }
        //}

        if (Input.GetMouseButtonUp(0)) {
            clicked = false;
            if (IslandDetailsPanel.activeSelf && 
                !RectTransformUtility.RectangleContainsScreenPoint(IslandDetailsPanel.transform.GetChild(0) as RectTransform, Input.mousePosition, Camera.main) && 
                Time.time-T1>0.5f &&
                Time.time - T0<0.1f) {
                // click outside the island panel which is open. So we're closing it.
                HideIslandDetailsPanel();
            }
        }

        if (clicked && !IslandRollover.dragging) {
            Vector2 delta = clicked_point - (Vector2)Input.mousePosition;
            (this.transform as RectTransform).anchoredPosition = anchored_position - delta;
        }

    }

    private Vector2 clicked_point;
    private Vector2 anchored_position;
    private bool clicked;

    /// <summary>
    /// Show island details panel.
    /// </summary>
    public void ShowIslandDetailsPanel(string name, int island_id, int team_id) {
        IslandRollover.current_island_id_selected = island_id;
        T1 = Time.time;
        IslandDetailsPanel.GetComponent<CanvasGroup>().alpha = 0f;
        IslandDetailsPanel.SetActive(true);
        // Set the title
        IslandDetailsPanel.transform.GetComponentsInChildren<TMP_Text>().Where(n=>n.name == "Island title").First().text = name;
        // Set its allegiance
        IslandDetailsPanel.GetComponentsInChildren<Toggle>()[team_id].isOn = true;
        TweenCGAlpha.Tween(IslandDetailsPanel, 0.5f, 1f);
        // Display the target around the island
        
    }
    private float T1;
    private float T0;

    /// <summary>
    /// Hide island details panel.
    /// </summary>
    public void HideIslandDetailsPanel() {
        IslandRollover.OnUnSelectIsland();
        IslandRollover.current_island_id_selected = -1;
        TweenCGAlpha.Tween(IslandDetailsPanel, 0.5f, 0f, () => {
            IslandDetailsPanel.SetActive(false);
        });
    }

    /// <summary>
    /// This method is triggered by Toggles in the Island details panels to change its allegiance.
    /// An event is thrown to change it properly.
    /// </summary>
    /// <param name="_team_id">the new team id of the selected island</param>
    public void ToggleIslandAllegianceChanged(int _team_id) {
        IslandRollover.OnAllegianceChanged(IslandRollover.current_island_id_selected, _team_id);
    }

}
